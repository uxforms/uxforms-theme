var gulp = require('gulp'),
    config = require('../config');


gulp.task('static:uxforms:images', function(){
  return gulp.src(config.SOURCE_DIR + '/static/images/**/*', { base: config.SOURCE_DIR + '/static' })
        .pipe(gulp.dest(config.TARGET_DIR));
});
gulp.task('static:uxforms:fonts', function(){
  return gulp.src(config.SOURCE_DIR + '/static/fonts/**/*', { base: config.SOURCE_DIR + '/static' })
    .pipe(gulp.dest(config.TARGET_DIR));
});

gulp.task('static:uxforms:icons', function(){
  return gulp.src(config.SOURCE_DIR + '/static/icons/**/*', { base: config.SOURCE_DIR + '/static' })
    .pipe(gulp.dest(config.TARGET_DIR));
});
gulp.task('static:uxforms:javascripts', function(){
  return gulp.src(config.SOURCE_DIR + '/static/javascripts/*', { base: config.SOURCE_DIR + '/static' })
        .pipe(gulp.dest(config.TARGET_DIR));
});

gulp.task('static:uxforms:templates', function(){
  return gulp.src(config.SOURCE_DIR + '/static/templates/**/*', { base: config.SOURCE_DIR + '/static' })
        .pipe(gulp.dest(config.TARGET_DIR));
});


gulp.task('static:vendor:jquery', function(){
	return gulp.src('./node_modules/jquery/dist/jquery.min.js')
				.pipe(gulp.dest(config.TARGET_DIR + '/javascripts/vendor/jquery'));
});


gulp.task('static',[
  'static:uxforms:images',
  'static:uxforms:fonts',
  'static:uxforms:icons',
  'static:uxforms:javascripts',
  'static:uxforms:templates',
  'static:vendor:jquery'
]);
