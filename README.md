# uxforms-theme

A theme for [UX Forms](https://uxforms.com), utlising UX Forms' own styles.

## Building

Requires `nodejs`.

The excellent [asdf-vm](https://asdf-vm.com/#/) (if you have it) will install the correct version for you, otherwise see
`.tool-versions` for the correct version to install.

### First run

`npm install` to install the project dependencies

### Build and package

`gulp`

Runs the `clean` and then `package` gulp tasks to:

* Delete the contents of the `target` dir
* Create all assets in the `target` dir
* Create a zip file of all of the assets in the `target` dir suitable to be deployed to UX Forms.

